CC                       := latexmk
BIBTEX                   := bibtex
OUTPUT_DIR               := "./build"
BASE_CFLAGS              := -f -xelatex -usepretex -shell-escape -output-directory=${OUTPUT_DIR}

LIBREOFFICE              := libreoffice
LIBREOFFICE_FLAGS        := --nologo --healess

PRESENTATION_PDF         := presentation
PRESENTATION_HELPER_PDF  := presentation_helper
META_REPORT_PDF          := meta_report
STATE_DIAGRAM_PDF        := state_diagram

LIBREOFFICE              := libreoffice
LIBREOFFICE_FLAGS        := --nologo --headless

define tex_tag_ctor_helper
	$(if $(filter presentation diploma diploma_full diploma_task_only review feedback practice economics,$1),\
		"\\usetag{$1}",\
	$(if $(filter presentation_helper,$1),\
		"",\
	$(if $(filter state_diagram,$1),\
		"",\
	$(error WRONG TAG <<<$1>>>))))
endef

define tex_tag_ctor
	-pretex="\AtBeginDocument{\usepackage{tagging}$(call tex_tag_ctor_helper,$1)}"
endef

define cflags_ctor
	${BASE_CFLAGS}\
	-jobname="$1"\
	$(call tex_tag_ctor,$(strip $1))\
	$(if $(filter diploma diploma_full diploma_task_only review feedback practice economics,$1),\
		$(META_REPORT_PDF),\
	$(if $(filter presentation,$1),\
		${PRESENTATION_PDF},\
	$(if $(filter presentation_helper,$1),\
		${PRESENTATION_HELPER_PDF},\
	$(if $(filter state_diagram,$1),\
		${STATE_DIAGRAM_PDF},\
	$(error WRONG TARGET NAME <<<$1>>>)))))
endef

define build_tex_cmd_ctor
	$(CC) $(call cflags_ctor,$(strip $1))
endef

define export_to_pdf_cmd_ctor
	$(LIBREOFFICE) ${LIBREOFFICE_FLAGS} --convert-to pdf $(strip $1) --outdir ${OUTPUT_DIR}
endef

.PHONY: all
all: practice diploma_full

.PHONY: state_diagram
state_diagram:
	@$(call export_to_pdf_cmd_ctor,"schemes/$@_back.odg")
	@$(call build_tex_cmd_ctor,$@)

.PHONY: scheme_01 scheme_02 scheme_03 screens uml
scheme_01 scheme_02 scheme_03 screens uml:
	@$(call export_to_pdf_cmd_ctor,"schemes/$@.odg")

.PHONY: documents_list
documents_list:
	@$(call export_to_pdf_cmd_ctor,"misc/$@.odt")

.PHONY: diploma practice economics diploma_task_only
presentation_helper diploma review feedback practice economics diploma_task_only: *.tex
	@$(call build_tex_cmd_ctor,$@)

.PHONY: diploma_full
diploma_full: state_diagram scheme_01 scheme_02 scheme_03 screens uml documents_list
	@$(call build_tex_cmd_ctor,$@)

.PHONY: presentation
presentation: scheme_01 scheme_02 scheme_03 screens uml
	@$(call build_tex_cmd_ctor,$@)

.PHONY: clean
clean:
	rm --recursive --force ${OUTPUT_DIR}
