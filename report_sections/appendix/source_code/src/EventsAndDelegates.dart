class ADelegate
{
  List<dynamic> _subscriptions = null;

  ADelegate() {
    this._subscriptions = new List<dynamic>();
  }

  ADelegate.fromCallable(dynamic callable) {
    this._subscriptions = new List<dynamic>();
    _subscriptions.add(callable);
  }

  ADelegate.fromSubscriptions(List<dynamic> subscriptions) {
    this._subscriptions = subscriptions;
  }

  ADelegate connect(dynamic callable) {
    return new ADelegate.fromSubscriptions(
        _subscriptions.toList()
          ..add(callable)
    );
  }

  ADelegate disconnect(dynamic callable) {
    var ptrIdx = _subscriptions.indexOf(callable);
    if (ptrIdx != -1) {
      return new ADelegate.fromSubscriptions(
          _subscriptions.toList()
            ..removeAt(ptrIdx)
      );
    }

    return this;
  }

  dynamic Invoke(List<dynamic> args) {
    dynamic retVal;

    _subscriptions.forEach((callable) {
      if (callable as Function != null) {
        retVal = Function.apply(callable, args);
      } else {
        retVal = callable(args);
      }
    });

    return retVal;
  }

  dynamic call(List<dynamic> args) {
    return Invoke(args);
  }

  ADelegate operator +(dynamic callable) {
    return connect(callable);
  }

  ADelegate operator -(dynamic callable) {
    return disconnect(callable);
  }

  bool operator ==(dynamic callable) {
    if (callable as Function != null) {
      return _subscriptions.length == 1 && _subscriptions.first == callable;
    } else {
      return _subscriptions == delegate._subscriptions; 
    }
  }
}

class AEvent 
{
  ADelegate _delegate = null;
  
  Function _get_delegate = null;
  Function _set_delegate = null;

  Function _add = null;
  Function _remove = null;

  ADelegate get corresponding_delegate => _get_delegate();
  
  AEvent() {
    this._delegate = new ADelegate();
    
    this._get_delegate = () { return _delegate; };
    this._set_delegate = (ADelegate delegate) { return _delegate = delegate; };

    this._add = (dynamic value) { _delegate = _delegate.connect(value); };
    this._remove = (dynamic value) { _delegate = _delegate.disconnect(value); };
  }
  
  AEvent.withInitializer(ADelegate initializer) {
    this._delegate = new ADelegate();
    
    this._get_delegate = () { return _delegate; };
    this._set_delegate = (ADelegate delegate) { return _delegate = delegate; };
    
    this._add = (dynamic value) { _delegate = _delegate.connect(value); };
    this._remove = (dynamic value) { _delegate = _delegate.disconnect(value); };

    connect(initializer);
  }

  AEvent.withAccessors(
      Function get_delegate,
      Function set_delegate,
      Function add_func,
      Function remove_func
  ) {
    this._get_delegate = get_delegate;
    this._set_delegate = set_delegate;
    
    this._add = add_func;
    this._remove = remove_func;
  }

  void connect(dynamic value) {
    this._add(value);
  }

  void exclusive_connect(dynamic callable) {
    this._set_delegate(new ADelegate.fromCallable(callable));
  }

  void disconnect(dynamic callable) {
    this._remove(callable);
  }

  dynamic _dispatch(List<dynamic> args) {
    return _get_delegate().emit(args);
  }

  AEventDispatcher get_dispatcher() {
    return new AEventDispatcher(this._dispatch);
  }

  operator +(dynamic callable) {
    return connect(callable);
  }

  operator -(dynamic callable) {
    return disconnect(callable);
  }

  bool operator ==(ADelegate delegate) {
    return this._get_delegate() == delegate;
  }
}

class AEventDispatcher 
{
  Function _dispatch_func;

  AEventDispatcher(Function dispatch_func) {
    _dispatch_func = dispatch_func;
  }
  
  dynamic dispatch(List<dynamic> args) {
    return _dispatch_func(args);
  }
  
  dynamic call(List<dynamic> args) {
    return dispatch(args);
  }
}
