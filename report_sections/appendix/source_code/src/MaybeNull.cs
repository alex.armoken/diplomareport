using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace Sharp2Dart.HelperProject
{
    public struct MaybeNull<T>
    {   
        internal T value;
        private bool hasValue;

        public MaybeNull(T value)
        {
            if (value == null)
            {
                this.value = default(T);
                this.hasValue = false;
            }
            else
            {
                this.value = value;
                this.hasValue = true;
            }
        }

        public bool HasValue => hasValue;

        public T Value
        {
            get
            {
                if (!hasValue)
                {
                    throw new NullReferenceException();
                }
                
                return value;
            }
        }

        public T GetValueOrDefault()
        {
            return value;
        }

        public T GetValueOrDefault(T defaultValue)
        {
            return hasValue ? value : defaultValue;
        }

        public override bool Equals(object other)
        {
            if (!hasValue)
            {
                return other == null;
            }

            return other != null && value.Equals(other);
        }

        public override int GetHashCode()
        {
            return hasValue ? value.GetHashCode() : 0;
        }

        public override string ToString()
        {
            return hasValue ? value.ToString() : "";
        }

        
        public bool Eq<TOther>(TOther obj)
        {
            if (this.HasValue)
            {
                return this.value.Equals(obj);
            }

            return false;
        }

        public bool Eq_N<TOther>(MaybeNull<TOther> obj) where TOther: struct
        {
            if (this.HasValue && obj.HasValue)
            {
                return this.value.Equals(obj.value);
            }
            else if (!this.HasValue && !obj.HasValue)
            {
                return true;
            }

            return false;
        }

        public bool Neq<TOther>(TOther obj)
        {
            return !this.Eq(obj);
        }

        public bool Neq_N<TOther>(MaybeNull<TOther> obj) where TOther: struct
        {
            return !this.Eq_N(obj);
        }
    }

    
    public static class Nullable
    {
        public static int Compare<T>(MaybeNull<T> n1, MaybeNull<T> n2) where T : struct
        {
            if (n1.HasValue) 
            {
                if (n2.HasValue)
                {
                    return Comparer<T>.Default.Compare(n1.value, n2.value);
                }

                return 1;
            }

            if (n2.HasValue)
            {
                return -1;
            }

            return 0;
        }            
        
        public static bool Equals<T>(MaybeNull<T> n1, MaybeNull<T> n2) where T : struct
        {
            if (n1.HasValue)
            {
                if (n2.HasValue)
                {
                    return EqualityComparer<T>.Default.Equals(n1.value, n2.value);
                }

                return false;
            }

            if (n2.HasValue)
            {
                return false;
            }
            
            return true;
        }

        public static Type GetUnderlyingType(Type nullableType) 
        {
            if((object)nullableType == null) 
            {
                throw new ArgumentNullException("nullableType");
            }
            Contract.EndContractBlock();
            Type result = null;

            if(nullableType.IsGenericType && !nullableType.IsGenericTypeDefinition) 
            { 
                Type genericType = nullableType.GetGenericTypeDefinition();
                if( Object.ReferenceEquals(genericType, typeof(Nullable<>))) 
                {
                    result = nullableType.GetGenericArguments()[0];
                }
            }

            return result;
        }
    }  
}