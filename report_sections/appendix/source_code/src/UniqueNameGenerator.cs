public static class UniqueNameGenerator
{
  private static bool IsMethodBody(SyntaxNode node)
  {
    if (node is BlockSyntax ||
        node is ArrowExpressionClauseSyntax)
    {
        return node.Parent is BaseMethodDeclarationSyntax ||
               node.Parent is AccessorDeclarationSyntax;
    }
    return false;
  }

  private static bool IsExecutableBlock(SyntaxNode node) => node.IsKind(SyntaxKind.Block);

  private static string GenerateNameForExpression(
    SemanticModel semanticModel, SyntaxNode expression, bool capitalize
  )
  {
    return semanticModel.GenerateNameForExpression(
      (ExpressionSyntax)expression, capitalize
    );
  }

  private static IEnumerable<IParameterSymbol> GetAnonymousMethodParameters(
    SemanticModel semanticModel, ExpressionSyntax expression
  )
  {
    var semanticMap = semanticModel.GetSemanticMap(expression);
    var anonymousMethodParameters = semanticMap.AllReferencedSymbols
        .OfType<IParameterSymbol>()
        .Where(p => p.ContainingSymbol.IsAnonymousFunction());
    return anonymousMethodParameters;
  }

  private static LambdaExpressionSyntax GetParentLambda(
    ExpressionSyntax expression, ISet<LambdaExpressionSyntax> lambdas
  )
  {
    var current = expression;
    while (current != null)
    {
      if (lambdas.Contains(current.Parent))
      {
        return (LambdaExpressionSyntax)current.Parent;
      }
      current = current.Parent as ExpressionSyntax;
    }
    return null;
  }

  private static bool IsInExpressionBodiedMember(ExpressionSyntax expression)
  {
    // walk up until we find a nearest enclosing block or arrow expression.
    for (SyntaxNode node = expression; node != null; node = node.GetParent())
    {
      // If we are in an expression bodied member and if the expression
      // has a block body, then, act as if we're in a block context and
      // not in an expression body context at all.
      if (node.IsKind(SyntaxKind.Block))
      {
        return false;
      }
      else if (node.IsKind(SyntaxKind.ArrowExpressionClause))
      {
        return true;
      }
    }

    return false;
  }

  private static SyntaxNode GetContainerToGenerateInto(
    SemanticModel semanticModel, ExpressionSyntax expression
  )
  {
    var anonymousMethodParameters = GetAnonymousMethodParameters(
      semanticModel,
      expression
    );
    var lambdas = anonymousMethodParameters
      .SelectMany(
        p => p.ContainingSymbol.DeclaringSyntaxReferences
                               .Select(r => r.GetSyntax()).AsEnumerable()
      )
      .OfType<LambdaExpressionSyntax>()
      .ToSet();

    var parentLambda = GetParentLambda(expression, lambdas);
    if (parentLambda != null)
    {
      return parentLambda;
    }
    else if (IsInExpressionBodiedMember(expression))
    {
      return expression.GetAncestorOrThis<ArrowExpressionClauseSyntax>();
    }
    else
    {
      return expression.GetAncestorsOrThis<BlockSyntax>().LastOrDefault();
    }
  }

  private static string GenerateUniqueName(
    string baseName, IEnumerable<ISymbol> candidates, 
    ImmutableHashSet<string> usedNames = null
  )
  {
    return NameGenerator.EnsureUniqueness(
      baseName, 
      candidates.Select(s => s.Name)
                .Concat(usedNames ?? ImmutableHashSet<string>.Empty), 
      isCaseSensitive: true
    );
  }

  private static string GenerateUniqueName(
    SemanticModel semanticModel,
    SyntaxNode location, SyntaxNode containerOpt,
    string baseName, Func<ISymbol, bool> filter, 
    ImmutableHashSet<string> usedNames = null
  )
  {
    var container = containerOpt ?? location.AncestorsAndSelf()
                                            .FirstOrDefault(
                                              a => IsExecutableBlock(a)
                                                || IsMethodBody(a)
                                            );
    var candidates = semanticModel.LookupSymbols(location.SpanStart)
      .Concat(semanticModel.GetExistingSymbols(container));
    return GenerateUniqueName(
      baseName, 
      filter != null ? candidates.Where(filter) : candidates,
      usedNames
    );
  }
        
  public static string GenerateUniqueName(
    SemanticModel semanticModel, SyntaxNode location, SyntaxNode containerOpt,
    string baseName, ImmutableHashSet<string> usedNames = null
  )
  {
    return GenerateUniqueName(
      semanticModel, location, containerOpt, baseName, 
      filter: null, usedNames: usedNames
    );
  }

  public static string GenerateUniqueLocalName(
    SemanticModel semanticModel, SyntaxNode location, SyntaxNode containerOpt,
    string baseName, ImmutableHashSet<string> usedNames = null
  )
  {
    // local name can be same as field or property. but that will hide
    // those and can cause semantic change later in some context.
    // so to be safe, we consider field and property in scope when
    // creating unique name for local
    Func<ISymbol, bool> filter = s => s.Kind == SymbolKind.Local 
                                      || s.Kind == SymbolKind.Parameter 
                                      || s.Kind == SymbolKind.RangeVariable 
                                      || s.Kind == SymbolKind.Field 
                                      || s.Kind == SymbolKind.Property;
    return GenerateUniqueName(semanticModel, location, containerOpt,
                              baseName, filter, usedNames);
  }

  public static string GenerateUniqueLocalName(
    SemanticModel semanticModel,
    ExpressionSyntax expression,
    bool isConstant,
    SyntaxNode containerOpt,
    ImmutableHashSet<string> usedNames = null
  )
  {
    var baseName = GenerateNameForExpression(semanticModel, expression,
                                             capitalize: isConstant);
    return GenerateUniqueLocalName(semanticModel, expression, containerOpt,
                                   baseName, usedNames);
  }
        
  public static string GenerateUniqueLocalName(
    SemanticModel semanticModel, ExpressionSyntax expression, 
    bool isConstant = false, ImmutableHashSet<string> usedNames = null
  )
  {
    var containerToGenerateInto = GetContainerToGenerateInto(
      semanticModel, expression
    );
    return GenerateUniqueLocalName(semanticModel, expression, isConstant,
                                   containerToGenerateInto, usedNames);
  }
}
